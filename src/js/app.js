/**
 * Angular JS Library.
 * @external "angular"
 * @see {@link https://angularjs.org/|Angular JS}
 */
angular
  /**
   * Angular JS application
   * @namespace external:"angular".ncsc_app
   * @author James Power
   */
  .module('ncsc.app', ['ngAnimate'])
  .run(function() {
    pipwerks.SCORM.init();
  })
  .service('BypassAnimation', [function() {
    return {
      bypass: false
    };
  }])
  .service('Progress', [function() {
    return {
      data: {
        current: undefined,
        completion: undefined,
        state: undefined
      },
      encode: function(data) {
        return JSON.stringify(data).replace(/"/g, '_');
      },
      decode: function(str) {
        return JSON.parse(str.replace(/_/g, '"'));
      },
      set: function() {
        console.log('progress:set ', this.data);
        if (pipwerks.SCORM.version) {
          pipwerks.SCORM.set('cmi.suspend_data', this.encode(this.data));
        }
        console.log('progress:set confirm: ', pipwerks.SCORM.get('cmi.suspend_data'));
      },
      get: function() {
        console.log(pipwerks.SCORM.get('cmi.mode'));
        var got = pipwerks.SCORM.get('cmi.suspend_data');
        console.log('GOT: ', got);
        if (typeof got === 'string' && got.length > 0 && pipwerks.SCORM.version !== null) {
          this.data = this.decode(got);
          return true;
          console.log('progress:get ', this.datachr);
        } else {
          return false;
          console.log('progress:get could not perform');
        }
      },
      save: function() {
        pipwerks.SCORM.save();
      },
      update: function(key, value) {
        if (key in this.data) {
          console.log('progress:update ', key, value);
          this.data[key] = value;
          return true;
        } else {
          console.log('progress:update could not perform');
          return false;
        }
      }
    };
  }])
  .service('VideoCompletion', ['$rootScope', 'Progress', function($rootScope, Progress) {

    Progress.get();

    return {
      contains: function(needle) {
        // Per spec, the way to identify NaN is that it is not equal to itself
        var findNaN = needle !== needle;
        var indexOf;

        if (!findNaN && typeof Array.prototype.indexOf === 'function') {
          indexOf = Array.prototype.indexOf;
        } else {
          indexOf = function(needle) {
            var i = -1,
              index = -1;

            for (i = 0; i < this.length; i++) {
              var item = this[i];

              if ((findNaN && item !== item) || item === needle) {
                index = i;
                break;
              }
            }

            return index;
          };
        }

        return indexOf.call(this, needle) > -1;
      },
      addCompletion: function(id) {
        var alreadyAdded = this.contains.call(this.completeList, id);

        if (alreadyAdded) {
          return false;
        } else {
          this.completeList.push(id);
          Progress.update('completion', this.completeList);
          Progress.set();
          Progress.save();

          if (id === 'video1') {
            $rootScope.$broadcast('welcome:completed', true);
          }

          if (this.completeList.length > 3) {
            this.scormComplete();
          }
          return true;
        }
      },
      completeList: Progress.data.completion || [],
      scormComplete: function() {
        // Set course completed / success statis

        pipwerks.SCORM.set('cmi.completion_status', 'completed');
        pipwerks.SCORM.set('cmi.success_status', 'passed');
        pipwerks.SCORM.save();
      }
    }
  }])
  /**
   * @class external:"angular".ncsc_app.getJson
   * @memberOf external:"angular".ncsc_app
   * @description Get JSON service
   * @return {object} appData
   * @author James Power
   * @see {@link http://stackoverflow.com/a/21590004/2689184}
   */
  .service('PageData', [
    '$http',
    function($http) {

      return {

        loadData: function() {
          return $http.get('data.json');
        },

        findPageByDeputyLink: function(data, link) {
          return _.find(data.pages, function(page) {
            return _.find(page.deputies, function(deputy) {
              return deputy.link === link;
            });
          });
        },

        findById: function(data, id) {
          return _.find(data.pages, function(page) {
            return page.id === id;
          });
        },

        findAllParents: function(data, id) {
          var page,
            parents = [],
            currentId = id;

          while ((page = this.findPageByDeputyLink(data, currentId)) != null) {
            parents.push(page);
            currentId = page ?
              page.id :
              undefined;
          }

          return parents.reverse();
        }

      };
    }
  ])
  /**
   * @class external:"angular".ncsc_app.AppController
   * @memberOf external:"angular".ncsc_app
   * @requires AppData
   * @description App controller
   * @author James Power
   */
  .controller('AppController', [
    'VideoCompletion',
    'PageData',
    '$scope',
    'SharedVars',
    'DeputyClick',
    '$timeout',
    'BypassAnimation',
    'Progress',
    '$window',
    function(VideoCompletion, PageData, $scope, SharedVars, DeputyClick, $timeout, BypassAnimation, Progress, $window) {
      var vm = this;

      vm.data = undefined;
      vm.current = undefined;
      vm.page = undefined;
      vm.direction = undefined;
      vm.state = undefined;
      vm.showFrame = false;
      vm.tl = new TimelineMax();
      vm.welcomeComplete = VideoCompletion.welcomeComplete;

      pipwerks.SCORM.set('cmi.exit', 'suspend');
      pipwerks.SCORM.save();

      vm.exit = function() {
        var api = pipwerks.SCORM.API.getHandle();

        pipwerks.SCORM.save();
        // pipwerks.SCORM.quit();
        // pipwerks quit method forces resetting of suspend_data
        if (api) {
          api.Terminate('');
        }
        $window.top.close();
      };

      vm.completedContains = function(id) {
        // console.log('Checking completition: ', id, ' ', VideoCompletion.completeList)
        return VideoCompletion.contains.call(VideoCompletion.completeList, id);
      };

      vm.setShowFrame = function(bool) {
        // console.log('set show frame: ', bool);
        vm.showFrame = bool;
      };

      vm.hasVideo = function(id) {
        if (id) {
          var linkedPage = PageData.findById(vm.data, id);

          if (linkedPage.video) {
            var videoId = linkedPage.video.id;

            if (vm.completedContains(videoId)) {
              return false;
            } else {
              return true;
            }

          } else {
            return false;
          }
        }
      };

      vm.goPage = function() {
        Progress.update('state', 'page');
        Progress.set();
        Progress.save();
        vm.state = 'page';
      };

      $timeout(function() {
        Progress.get();
        vm.state = Progress.data.state || 'welcome';
      });

      var getCurrent = function() {
        var current;
        Progress.get();
        current = Progress.data.current || 0;

        return current;
      };

      PageData
        .loadData()
        .then(function(response) {
          vm.data = response.data;
          vm.current = getCurrent();
          vm.direction = 'down';
        });

      $scope.$on('welcome:completed', function(event, bool) {
        $timeout(function() {
          Progress.update('state', 'instructions');
          Progress.set();
          Progress.save();
          vm.state = 'instructions';
        });
      });

      $scope.$on('media:ended', function(event, id) {
        $timeout(function() {
          vm.showFrame = false;

        });
        // console.log('media:ended received');
      });

      $scope.$watch('vm.current', function(current) {

        if (angular.isDefined(current)) {

          Progress.update('current', current);
          Progress.set();
          Progress.save();

          vm.parents = PageData.findAllParents(vm.data, current);
          vm.page = PageData.findById(vm.data, current);
          // if (angular.isDefined(vm.page.video)) {
          //   if (!vm.completedContains(vm.page.video.id)) {
          //     $timeout(function() {
          //       vm.showFrame = true;
          //     }, 400);
          //   } else {
          //     vm.showFrame = false;
          //   }
          // }
        }
      });

      vm.clickList = function() {
        // console.log('click');
        BypassAnimation.bypass = true;
      };

      vm.handleClick = function(id, $event) {
        var $circle = $($event.target).parents('.circle');
        BypassAnimation.bypass = false;

        if (angular.isDefined(id)) {
          $timeout(function() {
            vm.current = id;
          });


          if ($circle.length > 0) {
            vm.direction = 'down';
            DeputyClick.element = $circle;
          } else {
            vm.direction = 'up';
          }
        } else if (Modernizr.touchevents) {
          $circle.toggleClass('circle--spin');
        } else {
          return false;
        }
      };

    }
  ])
  .service('DeputyClick', function() {
    return {
      element: undefined
    };
  })
  .service('SharedVars', function() {
    return {
      velocity: 1,

      cy: '23rem',
      cx: '28.8rem',
      scale: 32 / 176,
      yOffset: '-=123',

      hide: function($ele) {
        TweenMax.set($ele, {
          autoAlpha: 0
        });
      },

      show: function($ele) {
        TweenMax.set($ele, {
          autoAlpha: 1
        });
      }
    };
  })
  .animation('.animation-fade', [
    'SharedVars',
    function(SharedVars) {
      return {
        enter: function(element, done) {
          var $ele = $(element);
          TweenMax.from($ele, SharedVars.velocity, {
            autoAlpha: 0,
            clearProps: 'all',
            onComplete: done
          });
        },
        leave: function(element, done) {
          var $ele = $(element);
          TweenMax.to($ele, SharedVars.velocity, {
            autoAlpha: 0,
            clearProps: 'all',
            onComplete: done
          });
        }
      };
    }
  ])
  .animation('.going-up', [
    'SharedVars',
    'BypassAnimation',
    function(SharedVars, BypassAnimation) {
      return {
        enter: function(element, done) {
          // console.log(BypassAnimation.bypass);
          if (BypassAnimation.bypass === true) {
            done();
            return;
          } else {
            // console.log('up enter');
            var $ele = $(element);
            var $head = $ele.find('.circle--head');
            var velocity = SharedVars.velocity;
            var cy = SharedVars.cy;
            var cx = SharedVars.cx;
            var show = SharedVars.show;
            var hide = SharedVars.hide;
            var yOffset = SharedVars.yOffset;
            var scale = SharedVars.scale;
            var $deputies = $ele.find('.circle--deputy');
            var $lines = $ele.find('.page__line');
            var tl = new TimelineMax({
              onComplete: done,
              onStart: function() {
                hide($deputies);
              }
            });

            tl
              .from($head, velocity, {
                scale: scale,
                y: yOffset
              })
              .from($deputies, velocity, {
                top: cy,
                left: cx,
                onStart: function() {
                  show($deputies);
                }
              })
              .from($lines, velocity, {
                width: 0
              }, velocity);
          }
        },
        leave: function(element, done) {
          // console.log(BypassAnimation.bypass);
          if (BypassAnimation.bypass === true) {
            done();
            return;
          } else {
            // console.log('up leave');
            var $ele = $(element);
            var velocity = SharedVars.velocity;
            var $deputies = $ele.find('.circle--deputy');
            var $nav = $ele.find('.page__nav');
            var $video = $ele.find('.video');
            var $head = $ele.find('.circle--head');
            var $title = $ele.find('.page__title');
            var $lines = $ele.find('.page__line');
            var hide = SharedVars.hide;
            var tl = new TimelineMax({
              onComplete: done,
              onStart: function() {
                hide($title);
                hide($nav);
                hide($video);
              }
            });

            tl.to([
              $deputies, $lines, $head
            ], velocity, {
              autoAlpha: 0
            });
          }
        }
      };
    }
  ])
  .animation('.going-down', [
    'DeputyClick',
    'SharedVars',
    'BypassAnimation',
    function(DeputyClick, SharedVars, BypassAnimation) {
      return {
        enter: function(element, done) {
          if (BypassAnimation.bypass === true) {
            done();
            return;
          } else {
            // console.log('down enter');
            var $ele = $(element);
            var $head = $ele.find('.circle--head');
            var $lines = $ele.find('.page__line');
            var $deputies = $ele.find('.circle--deputy');
            var velocity = SharedVars.velocity;
            var cy = SharedVars.cy;
            var cx = SharedVars.cx;
            var show = SharedVars.show;
            var hide = SharedVars.hide;
            var tl = new TimelineMax({
              onStart: function() {
                show($ele);
              },
              delay: velocity * 1,
              onComplete: done
            });

            hide($ele);

            tl
              .from($lines, velocity, {
                width: 0
              })
              .from($deputies, velocity, {
                top: cy,
                left: cx
              }, 0);
          }
        },
        leave: function(element, done) {
          if (BypassAnimation.bypass === true) {
            done();
            return;
          } else {
            // console.log('down leave');
            var $ele = $(element);
            if (angular.isUndefined(DeputyClick.element)) {
              TweenMax.set($ele, {
                autoAlpha: 0
              });
              done();
              return;
            } else {
              var $circle = DeputyClick.element;
              var velocity = SharedVars.velocity;
              var cy = SharedVars.cy;
              var cx = SharedVars.cx;
              var show = SharedVars.show;
              var hide = SharedVars.hide;
              var yOffset = SharedVars.yOffset;
              var scale = SharedVars.scale;
              var $head = $ele.find('.circle--head');
              var $headContent = $head.children();
              var $lines = $ele.find('.page__line');
              var $deputies = $circle.siblings('.circle--deputy');
              var tl = new TimelineMax({
                onComplete: done
              });

              tl
                .to($circle, velocity, {
                  top: cy,
                  left: cx
                })
                .to($head, velocity, {
                  scale: scale,
                  y: yOffset
                }, 0)
                .to($headContent, velocity, {
                  autoAlpha: 0
                }, 0)
                .to([
                  $lines, $deputies
                ], velocity / 2, {
                  autoAlpha: 0
                }, 0);
            }
          }
        }
      };
    }
  ])
  /**
   * The carousel directive
   * @class external:"angular".ncsc_app.appDirective
   * @return {object} Angular directive
   * @see {@link https://docs.angularjs.org/guide/directive}
   */
  .directive('appDirective', [function() {
    return {
      scope: {
        pages: '=',
        direction: '=',
        current: '=',
        setShowFrame: '=',
        showFrame: '=',
        handleClick: '=',
        parents: '=',
        hasVideo: '='
      },
      restrict: 'A',
      templateUrl: 'js/directive.html',
      link: function(scope, element) {

        scope.getDirectionClass = function() {
          return scope.direction == 'up' ? 'going-up' : 'going-down';
        };
      }
    };
  }])
  .directive('videoPlayer', [function() {
    return {
      scope: {
        video: '=',
        closable: '=',
        setShowFrame: '=',
        showFrame: '='
      },
      restrict: 'A',
      templateUrl: 'js/video.html',
      link: function(scope, ele, attrs) {}
    };
  }])
  .directive('mediaCompleter', ['VideoCompletion', '$rootScope', function(VideoCompletion, $rootScope) {
    return {
      restrict: 'A',
      link: function(scope, ele, attrs) {

        ele.on('ended', function(event) {

          var id = $(event.target).attr('data-id');
          VideoCompletion.addCompletion(id);

          $rootScope.$broadcast('media:ended', id);
        });
      }
    };
  }]);
