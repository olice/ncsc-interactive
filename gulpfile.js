// Dependencies
const mainBowerFiles = require('gulp-main-bower-files');
const gulp = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const rename = require('gulp-rename');
const sourcemaps = require('gulp-sourcemaps');
const minify = require('gulp-minify');
const concat = require('gulp-concat');
const jsdoc = require('gulp-jsdoc3');
const browserSync = require('browser-sync').create();
const gulpMerge = require('gulp-merge');
const scormManifest = require('gulp-scorm-manifest');
const zip = require('gulp-zip');

const staticFileTypes = '/**/*.{html,jpg,png,svg,gif,json,mp4,mp3}';

/**
 * Source directory
 * @type {String}
 */
const source = './src';

/**
 * Output directory
 * @type {String}
 */
const output = './public';

/**
 * App js files, as array for order to output into app.js
 * @type {Array}
 */
const frontendJsFiles = [
  source + '/js/modernizr.js',
  source + '/js/detect-browser.js',
  source + '/js/app.js'
];

/**
 * Config for gulp-minify
 * @type {Object}
 * @property {object} ext        - Extension config
 * @property {string} ext.src    - Extension for source (debug) output
 * @property {string} ext.min    - Extention for minified output
 * @property {array} exclude     - Any excluded folders
 * @property {array} ignoreFiles - Ignore patterns
 */
const minifyConfig = {
  ext: {
    src: '-debug.js',
    min: '.js'
  },
  exclude: ['tasks'],
  ignoreFiles: ['.combo.js', '-min.js']
};

const frontendJsDocConfig = {
  'tags': {
    'allowUnknownTags': true
  },
  'opts': {
    'destination': './docs/js/frontend'
  },
  'plugins': [
    'plugins/markdown'
  ],
  'templates': {
    'cleverLinks': true,
    'monospaceLinks': false,
    'default': {
      'outputSourceFiles': true
    },
    'path': 'ink-docstrap',
    'theme': 'cerulean',
    'navType': 'vertical',
    'linenums': true,
    'dateFormat': 'MMMM Do YYYY, h:mm:ss a'
  }
};

// SCORM manifest
gulp.task('scorm', function() {
  return gulp
    .src(output + '/**/*.*')
    .pipe(scormManifest({
      version: '2004',
      courseId: 'uk.org.cqc.academy.ncsc_interactive',
      SCOtitle: 'NCSC Organistion Chart',
      moduleTitle: 'NCSC Organistion Chart',
      launchPage: 'index.html',
      fileName: 'imsmanifest.xml'
    }))
    .pipe(gulp.dest(output));
});

// Zip
gulp.task('zip', () => {
  return gulp
    .src(output + '/**/*.*')
    .pipe(zip('ncsc_interactive-SCORM.zip'))
    .pipe(gulp.dest('./'));
});

gulp.task('move-other', function() {
  return gulp
    .src(source + staticFileTypes)
    .pipe(gulp.dest(output));
});

gulp.task('frontend-js', function() {
  return gulpMerge(
      gulp.src('./bower.json')
      .pipe(mainBowerFiles()),
      gulp.src(frontendJsFiles)
    )
    .pipe(concat('app.js'))
    .pipe(minify(minifyConfig)).pipe(gulp.dest(output + '/js/'));
});

gulp.task('frontend-js-docs', function() {
  return gulp.src(frontendJsFiles.concat(source + '/js/README.md'))
    .pipe(jsdoc(frontendJsDocConfig));
});

// SASS task
// Builds SCSS files to CSS
gulp.task('sass', function() {

  const sassOpts = {
    errLogToConsole: true,
    outputStyle: 'compressed'
  };

  return gulp
    .src(source + '/sass/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass(sassOpts).on('error', sass.logError))
    .pipe(autoprefixer({
      browsers: ['> 1%'] // version of browsers
    }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(output + '/css'))
    .pipe(browserSync.stream());
});

// Static Server + watching scss/html files
gulp.task('serve', ['sass'], function() {

  browserSync.init({
    server: './public'
  });

  gulp.watch(source + '/**/*.js', ['js']);
  gulp.watch(source + staticFileTypes, ['move-other']);
  gulp.watch(source + '/sass/**/*.scss', ['sass']);
  gulp.watch(!output + '/**/*.css').on('change', browserSync.reload);
});

gulp.task('js', ['frontend-js']);
gulp.task('docs', ['frontend-js-docs']);
gulp.task('build', ['move-other', 'js', 'docs', 'sass']);

// Static Server + watching scss/html files
gulp.task('default', ['build', 'serve']);
