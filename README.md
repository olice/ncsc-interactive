# CQC - NCSC Organisation Chart

## Installation

### Required Globals
```
npm i gulp -g
npm i bower -g
```

### Install
```
npm i
bower i
```


## Running
```
gulp
```

## Build for Production
```
gulp build && gulp scorm && gulp zip
```
Creates a zip file at `./ncsc_interactive-SCORM.zip` ready for upload to CQC LMS.

## App Structure and Data
The app is an Angular SPA. The app is compromised of two directives: `./src/js/directive.html` and `./src/js/video.html`. `Directive.html` creates the organisation hierarchy the `./src/data.json` file.

The shape of the app data is described below:

```cson
{
  "pages": [                            # Array of pages
    {
      "page": "Executive team",         # Page title
      "id": 0,                          # Page ID, used for links between pages
      "head": {                         # Head of department
        "name": "John Smith",
        "title": "Manager",
        "image": "img/image-file.jpg"   # Images are in './src/img/headshots/'
      },
      "video": { # Any department that requires a video has a video property
        "transcript": "video/transcript2.html", 
        "id": "video2",              
        "audio": {
          "src": "video/customer-contact.mp3"
        },
        "video": {
          "src": "video/customer-contact.mp4",
          "poster": "video/customer-contact.jpg"
        }
      },
      "deputies": [                     # Deputies are the subs to the head
        {
          "name": "A deputy",
          "title": "Deputy title",
          "image": "img/image-file.jpg"
        },
        {
          "name": "A deputy with a department",
          "title": "Deputy title",
          "link": 1                      # Deputies that are in charge of other departments (separate pages) have a link instead of an image. This number is the ID of the page
        }
      ]
    }
  ]
}
```
